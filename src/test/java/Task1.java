import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Task1 {

    @Test
    void Task1(){


        System.setProperty("webdriver.chrome.driver","C:/Users/HP/Desktop/chromedriver_win32/chromedriver.exe");

        WebDriver webDriver=new ChromeDriver();

try{
        webDriver.get("https://www.google.com");
        WebElement searchInput=webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
        searchInput.sendKeys("Selenium");


        WebElement searchButton=webDriver.findElement(By.className("gNO89b"));

        searchButton.click();

        WebElement seleniumLink=webDriver.findElement(By.cssSelector("#rso > div:nth-child(1) > div > div > div.yuRUbf > a"));
        seleniumLink.click();


    WebDriverWait wait=new WebDriverWait(webDriver,10) ;
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > section.hero.homepage > h1:nth-child(1)")));

    Assertions.assertTrue(webDriver.findElement(By.cssSelector("body > section.hero.homepage > h1:nth-child(1)")).isDisplayed());

}
finally{
    webDriver.close();
}
    }
}
