import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Task2 {
    @Test
    void Work2() {

        System.setProperty("webdriver.chrome.driver", "C:/Users/HP/Desktop/chromedriver_win32/chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        //leilatestmail@mail.ru
        //Leila1234

try{
        webDriver.get("https://mail.ru/");
        WebElement nameInput = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
        nameInput.sendKeys("leilatestmail@mail.ru");

        WebElement button1 = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
        button1.click();


        WebElement parolInput = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
        parolInput.sendKeys("Leila1234");
        WebElement button2 = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
        button2.click();
    WebDriverWait wait=new WebDriverWait(webDriver,20) ;
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__overlay > div > div.portal-menu.portal-menu_pony-mode.js-shortcut > span > div.layout__main-frame > table > tbody > tr > td.layout__sidebar.layout__sidebar_nowrap > div > div > div")));
    Assertions.assertTrue(webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__overlay > div > div.portal-menu.portal-menu_pony-mode.js-shortcut > span > div.layout__main-frame > table > tbody > tr > td.layout__sidebar.layout__sidebar_nowrap > div > div > div")).isDisplayed());


}
    finally{
    webDriver.close();

    }}
}
